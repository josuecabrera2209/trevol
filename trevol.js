
// Ejecutar funcion en el evento click

document.getElementById("btn_open").addEventListener("click",open_close_menu)

let menu_side = document.getElementById("menu_side");
let btn_open = document.getElementById("btn_open");
let body = document.getElementById("body");

// evento mostra ocultar menu

function open_close_menu (){
  body.classList.toggle("body_move");
  menu_side.classList.toggle("menu__side_move");
}


if (window.innerWidth < 760) {
  body.classList.add("body_move");
  menu_side.classList.add("menu__side_move");
}

// menu responsive

window.addEventListener("resize", function(){

  if (window.innerWidth > 760 ) {
    body.classList.remove("body_move");
    menu_side.classList.remove("menu__side_move")

  }

  if (window.innerWidth < 760 ) {
    body.classList.add("body_move");
    menu_side.classList.add("menu__side_move");

  }

});

// menu dropdawn

function menutoggle(){
  const togglemenu = document.querySelector('.menu');
  togglemenu.classList.toggle('active')
}


//////////////FILE///////////////////////

function buttonFirst(id) {
  const input2 = document.querySelector(id);
  input2.click();
}

